package com.litguard.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.litguard.main.litguards.R;

public class SplashViewPagerAdapter extends PagerAdapter {
    Activity context;
    LayoutInflater inflater;
    int[] arr = new int[]{R.drawable.h1, R.drawable.h2, R.drawable.h3};

    public SplashViewPagerAdapter(Activity act) {
        this.context = act;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.row_page_splash_activity, container,
                false);

        ImageView img = (ImageView) itemView.findViewById(R.id.introImg);
        img.setBackgroundResource(arr[position]);




        ((ViewPager) container).addView(itemView);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
