package com.litguard.main.adapter;

/**
 * Created by GSS-Vishnu Kant on 29/9/15.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.litguard.main.litguards.R;

public class CustomBannerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;

    int manufactureID[] = new int[]{53, 55, 2, 11, 29};
    String manufactureName[] = new String[]{"HIMALAYA", "JOHNSON", "OMRON", "VICHY", "PROTIEN"};


    public CustomBannerAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) ((Activity) mContext).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.banner_pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        imageView.setTag(position);


        if (position == 0) {
            imageView.setBackgroundResource(R.drawable.banner);
        } else if (position == 1) {
            imageView.setBackgroundResource(R.drawable.banner);
        } else if (position == 2) {
            imageView.setBackgroundResource(R.drawable.banner);
        } else if (position == 3) {
            imageView.setBackgroundResource(R.drawable.banner);
        } else if (position == 4) {
            imageView.setBackgroundResource(R.drawable.banner);
        }


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}