package com.litguard.main.litguards;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.litguard.main.fragment.ChatFragment;
import com.litguard.main.fragment.HomeFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private int width, height;
    private RelativeLayout rlCustomToolbar;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mGetResolutionDevice();

        mDrawerAndNavigationViewManage();

        loadHomeFragment();
    }


    private void mDrawerAndNavigationViewManage() {

        rlCustomToolbar = (RelativeLayout) findViewById(R.id.rlCustomToolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        params.width = (width / 4) + (width / 4) / 4;
        navigationView.setLayoutParams(params);

        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewAddressOnUser = vi.inflate(R.layout.row_slider_menu, null);

        viewAddressOnUser.findViewById(R.id.llMyChat).setOnClickListener(this);
        viewAddressOnUser.findViewById(R.id.llFindLawyer).setOnClickListener(this);
        navigationView.addView(viewAddressOnUser);

        findViewById(R.id.tvSliderMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });

    }

    private void loadHomeFragment() {
        closeDrawerUI();
        rlCustomToolbar.setBackgroundColor(getResources().getColor(R.color.trans));
        HomeFragment fragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment, "Home");
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    private void mGetResolutionDevice() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llFindLawyer:
                loadHomeFragment();

                break;
            case R.id.llMyChat:
                openChatFragment();

                break;
        }
    }


    private void openChatFragment() {
        closeDrawerUI();
        rlCustomToolbar.setBackgroundColor(getResources().getColor(R.color.buttonBg));

        ChatFragment fragment = new ChatFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment, "Chat");
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void closeDrawerUI() {
        drawer.closeDrawer(GravityCompat.START);
    }

}
