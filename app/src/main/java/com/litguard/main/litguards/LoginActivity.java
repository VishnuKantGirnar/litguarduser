package com.litguard.main.litguards;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.litguard.main.utils.Utility;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mFindWidget();
    }

    private void mFindWidget() {

        findViewById(R.id.rlLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.doStartActivityWithFinish(LoginActivity.this, MainActivity.class, "right");
            }
        });
    }

}
