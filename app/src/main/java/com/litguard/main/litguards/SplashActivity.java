package com.litguard.main.litguards;
/**
 * Splash Screen which have View Pager and login and signup
 *
 * @GirnarSez- Vishnu
 */

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.litguard.main.adapter.SplashViewPagerAdapter;
import com.litguard.main.custom.CirclePageIndicator;
import com.litguard.main.utils.Utility;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Find and assign adapter on View Pager
        mViewPagerManage();

    }//end onCreate===================


    /**
     * Viewpage initialize and set adapter
     */
    private void mViewPagerManage() {

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new SplashViewPagerAdapter(SplashActivity.this));

        CirclePageIndicator mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(pager);

    }//end mViewPagerManage=============

    public void loginProcess(View view) {
        Utility.doStartActivityWithFinish(SplashActivity.this, LoginActivity.class, "right");
    }

    public void signUpProcess(View view) {
        Utility.doStartActivityWithFinish(SplashActivity.this, LoginActivity.class, "right");
    }


}//end main class===========
