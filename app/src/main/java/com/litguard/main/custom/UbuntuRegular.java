package com.litguard.main.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by vishanu on 19/1/17.
 */
public class UbuntuRegular extends android.support.v7.widget.AppCompatEditText {

    public UbuntuRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public UbuntuRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UbuntuRegular(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Regular_1.ttf");
            setTypeface(tf);
        }
    }

}