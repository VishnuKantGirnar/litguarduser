package com.litguard.main.custom;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;


public class UbuntuRegularTextView extends TextView {


    public UbuntuRegularTextView(Context context) {
        super(context);
        if (isInEditMode()) {
            return;
        }
    }

    public UbuntuRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (isInEditMode()) {
            return;
        }
        setTypFace(context, attrs);

    }

    public UbuntuRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) {
            return;
        }
        setTypFace(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UbuntuRegularTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (isInEditMode()) {
            return;
        }
        setTypFace(context, attrs);
    }

    public void setTypFace(Context context, AttributeSet attrs) {

        //Typeface.createFromAsset doesn't work in the layout editor. Skipping...
        if (isInEditMode()) {
            return;
        }


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-Regular_1.ttf");
        setTypeface(typeface);
    }
}