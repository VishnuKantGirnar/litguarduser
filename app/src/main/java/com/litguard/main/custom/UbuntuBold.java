package com.litguard.main.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by vishanu on 19/1/17.
 */
public class UbuntuBold extends EditText {

    public UbuntuBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public UbuntuBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UbuntuBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Bold_1.ttf");
            setTypeface(tf);
        }
    }

}