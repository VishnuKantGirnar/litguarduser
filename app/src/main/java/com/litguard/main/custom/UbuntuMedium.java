package com.litguard.main.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by vishanu on 19/1/17.
 */
public class UbuntuMedium extends EditText {

    public UbuntuMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public UbuntuMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UbuntuMedium(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Medium_1.ttf");
            setTypeface(tf);
        }
    }

}