package com.litguard.main.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.litguard.main.litguards.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Contain all unique members
 */
public class Utility {

    public static Utility sUtility = null;
    public static Intent sIntent = new Intent();


    private Utility() {

    }

    public static Utility getUtilityInstance() {

        if (sUtility == null) {
            sUtility = new Utility();
        }
        return sUtility;
    }


    /**
     * return time interval in min
     */
    public int getTimeIntervalInMin(String firstTime, String LastTime) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
            return (int) ((format.parse(firstTime).getTime() - format.parse(LastTime).getTime()) / 60000);

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }//end getTimeIntervalInMin-----------------

    /**
     * return time interval in min
     */
    public int getTimeIntervalInHours(String firstTime, String LastTime) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return (int) ((format.parse(firstTime).getTime() - format.parse(LastTime).getTime()) / (60000 * 60 * 24));

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }//end getTimeIntervalInMin-----------------


    /**
     * return time interval in min
     */
    public float getTimeIntervalInMinFloat(String firstTime, String LastTime) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
            return (float) ((format.parse(firstTime).getTime() - format.parse(LastTime).getTime()) / 60000);

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }//end getTimeIntervalInMin-----------------


    /**
     * Get Current time
     */
    public static String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int a = c.get(Calendar.AM_PM);
        if (a == Calendar.AM)
            return hour + ":" + minute + " AM";
        else
            return hour + ":" + minute + " PM";
    }//end getCurrentTime------------


    public String getCurrentDateAndTime() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        String delegate = "hh:mm aaa";
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
        //txtFirstDateTime.setText(formattedDate + " " + (String) DateFormat.format(delegate, Calendar.getInstance().getTime()));

    }


    /**
     * show alert, title, message, single ok button
     */
    public void showAlertTMOK(Activity _activity, String title, String msg) {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(_activity);
            alert.setMessage("" + msg);
            alert.setPositiveButton("OK", null);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * return String from edittext with trim
     */
    public static String setStringCheckNull(String strData) {

        if (strData != null) {
            return strData.trim();
        } else {
            return "";
        }
    }

    /**
     * return String from edittext with trim
     */
    public String getStringFromEditText(EditText edData) {

        if (edData != null) {
            return edData.getText().toString().trim();
        } else {
            return "";
        }
    }

    /**
     * return String from textView with trim
     */
    public String getStringFromTextView(TextView edData) {

        if (edData != null) {
            return edData.getText().toString().trim();
        } else {
            return "";
        }
    }


    public String getMonthFixFormat(int month) {
        String place = "01";
        try {
            if ((month + 1) < 10) {
                place = "0" + (month + 1);
            } else {
                place = "" + (month + 1);
            }
        } catch (Exception e) {
        }
        return "" + place;
    }


    /**
     * get Device resolution
     */
    public DisplayMetrics getDeviceDimension(Activity _activity) {
        DisplayMetrics dimension = new DisplayMetrics();
        _activity.getWindowManager().getDefaultDisplay().getMetrics(dimension);
        return dimension;
    }


    /**
     * run progressDialog
     */
    static ProgressDialog mprogressDialog;

    public static void runProgressDialog(Activity _activity) {
        if (_activity != null && !_activity.isFinishing()) {
            mprogressDialog = ProgressDialog.show(_activity, null, "");
            mprogressDialog.setCanceledOnTouchOutside(false);
            //mprogressDialog.setContentView(R.layout.progress_dialog_blue);
            mprogressDialog.setCancelable(true);
        }
    }

    /**
     * stop progressDialog
     */
    public static void stopProgressDialog() {

        try {
            if (mprogressDialog != null) {
                mprogressDialog.dismiss();
                mprogressDialog = null;
            }
        } catch (Exception e) {

        }


    }


    /**
     * This method is used for get the connectivity status
     *
     * @return
     */
    public boolean getConnectivityStatus(Activity activity) {
        ConnectivityManager connManager = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connManager.getActiveNetworkInfo();
        if (info != null)
            if (info.isConnected()) {
                return true;
            } else {
                return false;
            }
        else
            return false;
    }


    public static void hideKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //show message
    public static void showMessage(String msg, Context con) {
        Toast.makeText(con, "" + msg, Toast.LENGTH_SHORT).show();
    }


    //check email Validation
    public static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    /*********************
     * set shared preferences
     **************************/
    public static void SetPreferences(Context con, String key, String value) {
        // save the data
        SharedPreferences preferences = con.getSharedPreferences("prefs_login",
                0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /******************
     * get shared preferences
     *******************/
    public static String getPreferences(Context con, String key) {
        SharedPreferences sharedPreferences = con.getSharedPreferences(
                "prefs_login", 0);
        String value = sharedPreferences.getString(key, "0");
        return value;

    }

    /**********************
     * set shared preferences in boolean
     *********************************/
    public static void SetPreferencesBoolean(Context con, String key, boolean value) {
        // save the data
        SharedPreferences preferences = con.getSharedPreferences("prefs_login",
                0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /*********************
     * get shared preferences in int
     ***********************/
    public static boolean getPreferencesBoolean(Context con, String key) {
        SharedPreferences sharedPreferences = con.getSharedPreferences(
                "prefs_login", 0);
        boolean value = sharedPreferences.getBoolean(key, false);
        return value;

    }


    /**********************
     * set shared preferences in int
     *********************************/
    public static void SetPreferencesInteger(Context con, String key, int value) {
        // save the data
        SharedPreferences preferences = con.getSharedPreferences("prefs_login",
                0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /*********************
     * get shared preferences in int
     ***********************/
    public static int getPreferencesInteger(Context con, String key) {
        SharedPreferences sharedPreferences = con.getSharedPreferences(
                "prefs_login", 0);
        int value = sharedPreferences.getInt(key, 0);
        return value;

    }

    public static int getPreferencesIntegerMinusOne(Context con, String key) {
        SharedPreferences sharedPreferences = con.getSharedPreferences(
                "prefs_login", 0);
        int value = sharedPreferences.getInt(key, -1);
        return value;

    }

    public static String getStringExtraProcess(String key, Activity context) {
        try {
            return context.getIntent().getStringExtra(key);
        } catch (Exception e) {
            return "";
        }
    }


    public static String getStringExtraBundle(String key, Activity context) {
        Bundle getBundle = null;
        try {
            getBundle = context.getIntent().getExtras();
            return getBundle.getString(key);
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }


    /*******
     * do animated activity
     ********/
    public static void doAnim(Activity act, String flag) {
        if (flag.equals("left")) {
            act.overridePendingTransition(R.anim.slide_in_left,
                    R.anim.slide_out_right);
        } else if (flag.equals("right")) {
            act.overridePendingTransition(R.anim.slide_in_right,
                    R.anim.slide_out_left);
        } else if (flag.equals("no")) {
            act.overridePendingTransition(0, 0);
        }
    }


    /**
     * start activity without finish, without any value
     */
    public static void doStartActivityWithoutFinish(Activity act,
                                                    Class cls, String anim_left_right_no) {
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity with finish, without any value
     */
    public static void doStartActivityWithFinish(Activity act,
                                                 Class cls, String anim_left_right_no) {
        act.startActivity(sIntent.setClass(act, cls));
        act.finish();
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity without finish, with String value
     */
    public static void doStartActivityWithoutFinishStringValue(Activity act,
                                                               Class cls, String key, String value, String anim_left_right_no) {
        sIntent.putExtra(key, value);
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity without finish, with 1 String value
     */
    public static void doStartActivityWithoutFinishString1Value(Activity act,
                                                                Class cls, String key1, String value1, String anim_left_right_no) {
        sIntent.putExtra(key1, value1);
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity with finish, with 1 String value
     */
    public static void doStartActivityWithFinishString1Value(Activity act,
                                                             Class cls, String key1, String value1, String anim_left_right_no) {
        sIntent.putExtra(key1, value1);
        act.startActivity(sIntent.setClass(act, cls));
        act.finish();
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity without finish, with 2 String value
     */
    public static void doStartActivityWithoutFinishString2Value(Activity act,
                                                                Class cls, String key1, String value1, String key2, String value2, String anim_left_right_no) {
        sIntent.putExtra(key1, value1);
        sIntent.putExtra(key2, value2);
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity without finish, with 3 String value
     */
    public static void doStartActivityWithoutFinishString3Value(Activity act,
                                                                Class cls, String key1, String value1, String key2, String value2, String key3, String value3, String anim_left_right_no) {
        sIntent.putExtra(key1, value1);
        sIntent.putExtra(key2, value2);
        sIntent.putExtra(key3, value3);
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity without finish, with int value
     */
    public static void doStartActivityWithoutFinishIntValue(Activity act,
                                                            Class cls, String key, int value, String anim_left_right_no) {
        sIntent.putExtra(key, value);
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }

    /**
     * start activity without finish, with Bundle
     */
    public static void doStartActivityWithoutFinishBundle(Activity act,
                                                          Class cls, Bundle bundle, String anim_left_right_no) {
        sIntent.putExtras(bundle);
        act.startActivity(sIntent.setClass(act, cls));
        doAnim(act, anim_left_right_no);
    }


    public static JSONObject getLocationFromAddress(String strAddressGeoCodingUrl) {

        try {
            URL url;
            HttpURLConnection urlConnection = null;
            JSONObject response = null;

            try {
                url = new URL(strAddressGeoCodingUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == 200) {
                    String responseString = readStream(urlConnection.getInputStream());
                    Log.v("TAG", responseString);
                    response = new JSONObject(responseString);
                } else {
                    Log.v("TAG", "Response code:" + responseCode);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }

            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

/*    public static void logoutUser(Context con) {

        Utility.SetPreferences(con, "Userid", "0");

        Intent intnt = new Intent(con, LoginActivity.class);
        // Closing all the Activities
        intnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        intnt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        con.startActivity(intnt);

        doAnim((Activity) con, "left");

    }*/

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "h:mm a";
        //String outputPattern = "dd-MMM-yyyy h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
