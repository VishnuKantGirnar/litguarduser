package com.litguard.main.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.litguard.main.adapter.CategoryAdapter;
import com.litguard.main.litguards.R;

public class ChatFragment extends Fragment {
    RecyclerView lllist;
    View rootView;
    private Activity _activity = getActivity();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_chat, container, false);
            try {
                initialiseView();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return rootView;
        } else {
            if (rootView.getParent() != null) {
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
            return rootView;
        }
    }


    private void initialiseView() {
        lllist = (RecyclerView) rootView.findViewById(R.id.list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(_activity, LinearLayoutManager.VERTICAL, false);
        lllist.setLayoutManager(mLayoutManager);
        lllist.setItemAnimator(new DefaultItemAnimator());
        lllist.setAdapter(new CategoryAdapter());

    }

}
