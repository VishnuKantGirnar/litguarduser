package com.litguard.main.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;

import com.litguard.main.adapter.CategoryAdapter;
import com.litguard.main.adapter.CustomBannerAdapter;
import com.litguard.main.custom.CirclePageIndicator;
import com.litguard.main.litguards.R;

import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {
    RecyclerView lllist;
    View rootView;
    private Activity _activity = getActivity();
    private RelativeLayout mFrameMain;
    private Handler mHandlerOld;
    private ViewPager pagerBanner;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_home, container, false);
            try {
                initialiseView();
                setData();
                pageSwitcher(8);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return rootView;
        } else {
            if (rootView.getParent() != null) {
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
            return rootView;
        }
    }


    private void setData() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(_activity, LinearLayoutManager.VERTICAL, false);
        lllist.setLayoutManager(mLayoutManager);
        lllist.setItemAnimator(new DefaultItemAnimator());
        lllist.setAdapter(new CategoryAdapter());

    }

    private void initialiseView() {
        lllist = (RecyclerView) rootView.findViewById(R.id.list);
        mHandlerOld = new Handler();

        pagerBanner = (ViewPager) rootView.findViewById(R.id.pagerBanner);
        pagerBanner.setAdapter(new CustomBannerAdapter(getActivity()));

        CirclePageIndicator mIndicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        mIndicator.setViewPager(pagerBanner);

    }


    Timer timer;
    int page = 1;

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }

    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    if (page >= 4) { // In my case the number of pages are 5
                        page = 0;
                        pagerBanner.setCurrentItem(page++);

                    } else {
                        pagerBanner.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    public class FixedSpeedScroller extends Scroller {

        private int mDuration = 3000;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, interpolator, flywheel);
        }


        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();

    }


}
